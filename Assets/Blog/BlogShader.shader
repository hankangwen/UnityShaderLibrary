﻿Shader "Blog/BlogShader"
{
    Properties
    {
        _Color("Totally Rad Color", Color) = (1, 1, 1, 1)   // 颜色
        _MainTexture("Main Texture", 2D) = "white" {}
        _DissolveTexture("Dissolve Texture", 2D) = "white" {}
        _DissolveCutoff("Dissolve Cutoff", Range(0, 1)) = 1
        _ExtrudeAmount("Extrude Amount", float) = 0
    }
    SubShader
    {
        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            
            #include "UnityCG.cginc"

            // 从CG中获取属性
            float4 _Color;
            sampler2D _MainTexture;
            sampler2D _DissolveTexture;
            float _DissolveCutoff;
            float _ExtrudeAmount;

            struct a2v{
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
                float3 normal : NORMAL;
            };

            struct v2f{
                float4 position : SV_POSITION;
                float2 uv : TEXCOORD0;
            };

            v2f vert(a2v v){
                v2f o;

                // _Time是一个时间的变量被包含在UnityCH.cginc中，y代表秒
                v.vertex.xyz += v.normal.xyz * _ExtrudeAmount * sin(_Time.y);
                
                // 将世界空间的模型坐标转换到裁剪空间
                o.position = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;

                return o;
            }

            fixed4 frag(v2f i) : SV_TARGET{
                float4 textureColor = tex2D(_MainTexture, i.uv);
                float4 dissolveColor = tex2D(_DissolveTexture, i.uv);
                clip(dissolveColor.rgb - _DissolveCutoff);

                return textureColor * _Color;
                // return tex2D(_MainTexture, i.uv);
            }

            ENDCG
        }
    }
}
