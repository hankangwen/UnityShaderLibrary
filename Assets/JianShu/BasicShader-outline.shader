﻿// 在模型外画出边框
Shader "JianShu/BasicShader-outline"
{
    Properties
    {
        _Texture("Texture", 2D) = "white" {}
        _OutLineColor("OutLineColor", Color) = (0, 0, 1, 1)
        _Scale("Scale", Range(0, 0.05)) = 0.02
    }
    SubShader
    {
        Tags{"Queue" = "Geometry"  "RenderType" = "Opaque"}
        LOD 100
        Pass
        {
            Cull Front    
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            
            fixed4 _OutLineColor;
            fixed _Scale;

            struct a2v{
                fixed4 vertex : POSITION;
                fixed4 normal : NORMAL;
            };
            struct v2f{
                fixed4 vertex : SV_POSITION;
            };

            v2f vert(a2v v){
                v2f o;
                v.vertex.xyz += v.normal * _Scale;
                o.vertex = UnityObjectToClipPos(v.vertex);                
                return o;
            }

            fixed4 frag(v2f i) : SV_TARGET{
                return _OutLineColor;
            }
            ENDCG
        }

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            sampler2D _Texture;

            struct a2v{
                fixed4 vertex : POSITION;
                fixed2 uv : TEXCOORD0;
            };
            struct v2f{
                fixed4 vertex : SV_POSITION;
                fixed2 uv : TEXCOORD0;
            };

            v2f vert(a2v v){
                v2f o;
                o.uv = v.uv;
                o.vertex = UnityObjectToClipPos(v.vertex);
                return o;
            }

            fixed4 frag(v2f i) : SV_Target{
                return tex2D(_Texture, i.uv);
            }

            ENDCG
        }
    }
}
