﻿Shader "JianShu/BasicShader_Normal"
{
    Properties
    {
        _Texture("Texture", 2D) = "white" {}
        _Color("Color", Color) = (1, 1, 1, 1)
        _Pos("Pos", Range(-1, 1)) = -0.3
        _Range("Range", Range(0, 2)) = 0.2
        _Scale("Scale", Range(0, 0.5)) = 0.2
    }
    SubShader
    {
        Pass
        {
            Tags {"Queue" = "Geometry"  "RenderType" = "Opaque"}
            LOD 100

            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            
            sampler2D _Texture;
            fixed4 _Color;
            fixed _Pos;
            fixed _Range;
            fixed _Scale;

            struct a2v{
                fixed4 vertex : POSITION;
                fixed2 uv : TEXCOORD0;
                fixed4 normal : NORMAL;
            };
            struct v2f{
                fixed4 vertex : SV_POSITION;
                fixed2 uv : TEXCOORD0;
                fixed4 color : COLOR;
            };

            v2f vert(a2v v){
                v2f o;
                o.uv = v.uv;
                if(v.vertex.y <= _Pos && v.vertex.y >= _Pos - _Range){
                    o.color = _Color;
                    v.vertex.xyz +=v.normal * _Scale;
                }
                else{
                    o.color = fixed4(1, 1, 1, 1);
                }
                o.vertex = UnityObjectToClipPos(v.vertex);
                return o;
            }

            fixed4 frag(v2f i) : SV_Target{
                return tex2D(_Texture, i.uv) * i.color;
            }

            ENDCG
        }
    }
}
