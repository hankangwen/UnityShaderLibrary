﻿Shader "Sbin/ff1"{
    Properties{
        _Color("Main Color", Color) = (1, 1, 1, 1)
        _Ambient("Ambient", Color) = (0.3, 0.3, 0.3, 0.3)
        _Specular("Specular", Color) = (1, 1, 1, 1)
        _Shininess("Shininess", Range(0, 0.8)) = 0.4
        _Emission("Emission", Color) = (1, 1, 1, 1)
        _MainTex("MainTex", 2D) = "white" {}
        _SecondTex("SecondTex", 2D) = "white" {}
        _Constant("ConstanColor", Color) = (1, 1, 1, 0.3)
    }
    SubShader{
        Tags {"Queue" = "Transparent"}
        Pass{
            Blend SrcAlpha OneMinusSrcAlpha   // Alpha blending
            material{
                diffuse[_Color]
                ambient[_Ambient]
                specular[_Specular]
                shininess[_Shininess]
                emission[_Emission]
            }
            lighting on
            separatespecular on

            settexture[_MainTex]{
                combine texture * primary double
            }
            settexture[_SecondTex]{
                constantColor[_Constant]
                combine texture * previous double, texture * constant
            }
        }
    }
}
