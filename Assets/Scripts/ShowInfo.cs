﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Profiling;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System.Text;

public class ShowInfo : MonoBehaviour {
	public float updateInterval = 0.5F;
    public bool debug = false;

    private float lastInterval;
	private int frames = 0; // Frames drawn over the interval
	private string fpsText;
	private string drawCallsText;
	private GUIStyle style;
    private GUIStyle memoryStyle;
    private Rect position;
    private Rect position2;

    //不同等级的DC范围，在范围内黄色警告，大于则红色警告
    //Low/Medium/High/Higher
    private int[,] dcThresholds = new int[4,2] { { 90, 95 }, { 110, 140 }, { 120, 170 }, { 150, 200 }};

    void Awake()
    {
        this.enabled =  Debug.isDebugBuild || Application.isEditor;
    }

    void Start()
    {
        lastInterval = Time.realtimeSinceStartup;
        style = new GUIStyle();
        style.normal.textColor = Color.black;
        style.normal.background = new Texture2D(80, 18);
        memoryStyle = new GUIStyle();
        memoryStyle.fontSize = 24;
        memoryStyle.normal.textColor = Color.black;
#if UNITY_EDITOR
        style.fontSize = GetFontSize();
        position = new Rect(50.0f, 0.0f, style.fontSize * 3.5f, style.fontSize);
        position2 = new Rect(50.0f + style.fontSize * 3.5f, 0.0f, style.fontSize * 2.5f, style.fontSize);
#else
        //style.fontSize = 40;
        //position = new Rect(50.0f, 0.0f, 140.0f, 50.0f);
        style.fontSize = GetFontSize();
        position = new Rect(50.0f, 0.0f, style.fontSize * 3.5f, style.fontSize);
        position2 = new Rect(50.0f + style.fontSize * 3.5f, 0.0f, style.fontSize * 2.5f, style.fontSize);
#endif
    }

    int GetFontSize()
    {
        if (Screen.width <= 800) {
            return 16;
        } else if (Screen.width <= 1280) {
            return 20;
        } else if (Screen.width <= 1920) {
            return 28;
        } else {
            return 38;
        }
    }

    void CalculateFPS()
	{
		++frames;
        float timeNow = Time.realtimeSinceStartup;
		// Interval ended - update GUI text and start new interval
		if (timeNow > lastInterval + updateInterval)
		{
            int level = QualitySettings.GetQualityLevel();
            var name = QualitySettings.names[level];

            // display two fractional digits (f2 format)
            float fps = frames/(timeNow - lastInterval);
			string format = System.String.Format("{0}FPS {1:F0}FPS-", Application.targetFrameRate, fps);
			fpsText = format;
            lastInterval = timeNow;
			frames = 0;

            if (fps < 25) {
                style.normal.textColor = Color.red;
            } else if (fps < 27) {
                style.normal.textColor = Color.yellow;
            } else {
                style.normal.textColor = Color.black;
            }

            CalculateDrawCalls();
        }
	}
	void CalculateDrawCalls()
	{
#if UNITY_EDITOR
         int index = QualitySettings.GetQualityLevel();
         var drawCalls = UnityStats.drawCalls;
         if (drawCalls < dcThresholds[index,0]) {
             drawCallsText = System.String.Format("<color=black>{0}dc</color>", drawCalls);
         } else if (drawCalls < dcThresholds[index,1]) {
             drawCallsText = System.String.Format("<color=yellow>{0}dc</color>", drawCalls);
         } else {
             drawCallsText = System.String.Format("<color=red>{0}dc</color>", drawCalls);
         }
        if (debug && UnityStats.setPassCalls > dcThresholds[index, 1])
        {
            Debug.LogError("别玩了，dc爆炸了，赶紧检查一下！！！");
            EditorApplication.isPaused = true;
        }

        //reset fontSize
        style.fontSize = GetFontSize();
        position = new Rect(50.0f, 0.0f, style.fontSize * 7f, style.fontSize);
        position2 = new Rect(50.0f + style.fontSize * 7f, 0.0f, style.fontSize * 2.5f, style.fontSize);
#endif
    }

	// Update is called once per frame
	void Update () {
	    CalculateFPS();
	}

    private Rect memoryPos = new Rect(200, 50, 150, 50);

    void OnGUI() {
		GUI.Label(position, fpsText, style);
        var memoryB = Profiler.GetTotalAllocatedMemoryLong() / 1024 / 1024;
        GUI.Label(memoryPos, "内存占用：" + memoryB + "MB", memoryStyle);
#if UNITY_EDITOR
        GUI.Label(position2, drawCallsText, style);
#endif
    }
}
