﻿Shader "TA/ChangeColor"
{
    Properties
    {
        _Color("Color", Color) = (1, 1, 1, 1)
    }
    SubShader
    {
        Pass
        {
            CGPROGRAM
            // 先定义好顶点着色器与片段着色器
            // #pragma -- Unity内置的编译指令用的命令
            #pragma vertex vert
            #pragma fragment frag

            fixed4 _Color;      // 使用变量需要重新声明，float\half\fixed

            struct appdata      // 应用阶段的结构体
            {
                float4 vertex:POSITION;
                float2 uv:TEXCOORD;
            };
            struct v2f          // 顶点着色器传递给片元着色器的结构体
            {
                float4 pos:SV_POSITION;
                float2 uv:TEXCOORD;
            };

            v2f vert(appdata v)
            {
                v2f o;
                o.pos = UnityObjectToClipPos(v.vertex); 
                o.uv = v.uv;
                return o;
            }

            fixed checker(float2 uv)    // 自定义函数
            {
                float2 repeatUV = uv * 10;
                float2 c = floor(repeatUV) / 2;
                float checker = frac(c.x + c.y) * 2;
                return checker;
            }

            // SV_TARGET就是指定输出颜色到RenderTarget的语义
            float4 frag(v2f i) : SV_TARGET
            {
                fixed col = checker(i.uv);
                return col;
            }
            ENDCG
        }
    }
    // Fallback "Diffuse"      // 回退
    // CustomEditor "EditorName"   // 自定义界面
}
// 结语：
// 顶点着色器与片断着色器的执行并不是1:1的，
// 举个例子，一个三角面片，只有三个顶点，顶点着色器只需执行3次，
// 而片断着色器由最终的像素数来决定，执行几百上千都是很正常的。
// 尽量把计算放在顶点着色器中去执行。其次在片断着色器中也要尽量的简化算法，节省开支。

// Cg/HLSL中几种常见的数据类型
// 1.float/half/fixed(三个都是浮点型)，PC默认都为float
// 2.integer(整形)
// 3.sampler2D(2D纹理)
// 4.samplerCUBE(3D纹理)

// Properties与Cg/HLSL对应
// 1. Int/float/Range  对应    float/half
// 2. Vector/Color  对应    float4/half4
// 3. 2D    对应    sampler2D
// 4. 3D    对应    sampler3D
// 5. CUBE  对应    samplerCUBE