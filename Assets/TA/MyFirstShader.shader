﻿Shader "TA/MyFirstShader"   // 下拉列表的路径
{
    Properties              // 属性
    {
        // [Attribute]_Name("Display Name", Type) = Default Value
        // [HDR] 使颜色亮度超过1，这个值配合镜头Bloom做出物体泛光效果
        _Color("Color", Color) = (1, 1, 1, 0.5)
        // 小数也会被识别成整数（向下取值）
        _Int("Int", Int) = 1
        _Float("Float", Float) = 0.5
        _FloatRange("Float Range", Range(0, 1)) = 0.5
        [PowerSlider(3)]_FloatRangePS("FloatRangePS", Range(0, 1)) = 0.5
        [IntRange]_FloatIntRange("FloatIntRange", Range(0, 1)) = 1
        [Toggle]_FloatToogle("FloatToogle", Range(0, 1)) = 1
        [Enum(UnityEngine.Rendering.CullMode)]_FloatEnum("FloatEnum", Float) = 1
        _Vector("Vector", Vector) = (0, 0, 0, 0)         // 四维向量
        _MainTex("2D纹理", 2D) = "white" {}            // 2D纹理贴图
        [NoScaleOffset]_MainTexNoSO("2D纹理NoSO", 2D) = "white" {}            // 2D纹理贴图
        // 默认值：white    black   gray    bump(法线图)
        [Normal]_MainTexNormal("2D纹理Normal", 2D) = "white" {}            // 法线贴图
        // 3D纹理主要用在查找表或者体积数据上,只显示灰色图
        _MainTex3D("3D纹理", 3D) = ""{}
        // Cube纹理，一个被拆开的立方体。
        [Header(This is a cubeTex)]_MainTexCube("cube纹理", CUBE) = "" {}


        // Other
        // 1. [HideInInspector]
        // 2. 
    }
    SubShader               // SubShaders
    {
        // 在PASS中添加Cg/HLSL代码片段实现，CGPROGRAM开始   ENDCG结束
        Pass                //  渲染一次模型
        {
        }
    }
    Fallback "Diffuse"      // 回退
    CustomEditor "EditorName"   // 自定义界面
}
